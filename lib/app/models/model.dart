import 'package:flutter/material.dart';

/**
 * Created by Bayu Nugroho
 * Copyright (c) 2021 . All rights reserved.
 */

class ProductModel {
  String? name;
  String? image;
  int? price;
  int? qty;

  ProductModel({this.name, this.price, this.qty,this.image});

}

class PriceModel {
  int? price;
  int? color;

  PriceModel({ this.price,this.color});
}