import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:vending_machine/app/modules/home/views/home_view.dart';
import 'package:vending_machine/app/utils/colors.dart';
import 'package:vending_machine/app/widgets/widget_intro.dart';

import '../controllers/intro_controller.dart';

class IntroView extends GetView<IntroController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(dark),
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Color(dark),
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 20, top: 20),
            child: InkWell(
              onTap: (){
                Get.offAll(HomeView());
              },
              child: Text('Skip', style: TextStyle(
                  color: Color(white),
                  fontSize: 18,
                  fontWeight: FontWeight.w400
              ),),
            ),
          )
        ],
      ),

      body: Stack(
        alignment: Alignment.bottomCenter,
        children: <Widget>[
          PageView(
            onPageChanged: (int page) {
              controller.currentIndex.value = page;
            },
            controller: controller.pageController,
            children: <Widget>[
              makePageIntro(
                  image: 'assets/img/intro1.png',
                  title: "Vending Machine",
                  content: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. "
              ),
              makePageIntro(
                  reverse: true,
                  image: 'assets/img/intro3.png',
                  title: "Vending Machine",
                  content: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. "
              ),
              makePageIntro(
                  image: 'assets/img/intro2.png',
                  title: "Vending Machine",
                  content: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. "
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.only(bottom: 60),
            child: Obx(()=>Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: controller.buildIndicator(),
            )),
          )
        ],
      ),
    );
  }}
