import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:vending_machine/app/models/model.dart';
import 'package:vending_machine/app/routes/app_pages.dart';

class DetailController extends GetxController {
  final listPrice = <PriceModel>[].obs;
  TextEditingController priceController = new TextEditingController();
  TextEditingController qtyController = new TextEditingController();

  final count = 0.obs;
  final money = 0.obs;
  final pacyback = 0.obs;
  @override
  void onInit() {
    addDataPrice();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void addDataPrice() {
    listPrice.add(new PriceModel(
        price: 6000,
        color: 1
    ));

    listPrice.add(new PriceModel(
        price: 8000,
        color: 0
    ));

    listPrice.add(new PriceModel(
        price: 10000,
        color: 0
    ));
    listPrice.add(new PriceModel(
        price: 12000,
        color: 0
    ));
    listPrice.add(new PriceModel(
        price: 15000,
        color: 0
    ));
    update();
  }

  void changeColor(int index, ProductModel? product){
    for(int i =0;i<listPrice.length;i++){
      listPrice[i].color = 0;
      listPrice[index].color = 1;
      qtyController.text = "0";
      count.value = 0;
      money.value = listPrice[index].price!;
      pacyback.value = listPrice[index].price! - product!.price!;
      listPrice.refresh();
      update();

    }
  }

  void checkout(int money, ProductModel? product){
    if(product!.qty! < 1){
      Get.snackbar("opps maaf", "stok sedang habis :(");
    }else if(money < 0) {
      Get.snackbar("opps", "transaksi gagal, uang anda kurang");
    }      else{
        Fluttertoast.showToast(
            msg: "Success,silahkan ambil barang anda",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );

        product.qty = product.qty! -1;
        listPrice.refresh();
        update();
        Get.offAndToNamed(Routes.HOME);
      }
    }


}

