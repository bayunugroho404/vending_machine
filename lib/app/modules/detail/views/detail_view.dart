import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:vending_machine/app/models/model.dart';

import '../controllers/detail_controller.dart';

class DetailView extends GetView<DetailController> {
  ProductModel? product;

  DetailView({this.product});

  final controller = Get.put(DetailController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('${product!.name}'),
        centerTitle: true,
      ),
      body: Obx(() => Padding(
            padding: const EdgeInsets.all(8.0),
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width,
                    color: Colors.blue,
                    height: MediaQuery.of(context).size.height / 10,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Detail Product',
                          style: TextStyle(fontSize: 20),
                        ),
                        Text(
                          'Price : Rp.${product!.price}/pcs',
                          style: TextStyle(fontSize: 20),
                        ),
                        Text(
                          'Stok : ${product!.qty}',
                          style: TextStyle(fontSize: 20),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    'Money',
                    style: TextStyle(fontSize: 20),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height / 1.8,
                    child: GridView.builder(
                        gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                            maxCrossAxisExtent: 200,
                            childAspectRatio: 3 / 2,
                            crossAxisSpacing: 20,
                            mainAxisSpacing: 20),
                        itemCount: controller.listPrice.length,
                        itemBuilder: (BuildContext ctx, index) {
                          return InkWell(
                            onTap: () {
                              controller.changeColor(index, product);
                            },
                            child: Container(
                              alignment: Alignment.center,
                              child: Text(
                                "Rp.${controller.listPrice[index].price}",
                                style: TextStyle(color: Colors.white),
                              ),
                              decoration: BoxDecoration(
                                  color: controller.listPrice[index].color == 0
                                      ? Colors.grey
                                      : Colors.blue,
                                  borderRadius: BorderRadius.circular(15)),
                            ),
                          );
                        }),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height / 6,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Obx(() => Text(
                                  'Money : Rp.${controller.money.value}',
                                  style: TextStyle(fontSize: 15),
                                )),
                            SizedBox(
                              height: 4,
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            Obx(() => Text(
                                  'Uang Kembali : ${controller.pacyback.value}',
                                  style: TextStyle(fontSize: 15),
                                )),
                          ],
                        ),
                        Spacer(),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: ElevatedButton(
                              onPressed: () {
                                controller.checkout(controller.pacyback.value,product);
                              },
                              style: ElevatedButton.styleFrom(
                                shape: new RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(30.0),
                                ),
                              ),
                              child: Text('  Checkout   ')),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          )),
    );
  }
}
