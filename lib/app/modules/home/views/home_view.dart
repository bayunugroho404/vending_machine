import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:vending_machine/app/modules/detail/views/detail_view.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {

  final controller = Get.put(HomeController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text('Vending Machine'),
        centerTitle: true,
      ),
      body: GridView.builder(
        shrinkWrap: true,
        primary: false,
        itemCount: controller.listProduct.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          mainAxisSpacing: 3.0,
          childAspectRatio: 1,
        ),
        itemBuilder: (
            context,
            index,
            ) {
          return Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            elevation: 4,
            child: GestureDetector(
              onTap: () {
                  Get.to(DetailView(
                    product :controller.listProduct[index]
                  ));
                },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Flexible(
                    flex: 2,
                    child: Container(
                      decoration: BoxDecoration(
                          border:
                          Border.all(color: Colors.white)),
                      child: Image.asset(
                        controller.listProduct[index].image!,
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                  Flexible(
                    flex: 1,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      child: Text(
                        controller.listProduct[index].name! +
                            "\n" +
                            "Rp." +
                            "${controller
                                .listProduct[index].price} (${controller.listProduct[index].qty})",
                        textAlign: TextAlign.center,
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
