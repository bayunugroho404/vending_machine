import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:vending_machine/app/models/model.dart';

class HomeController extends GetxController {
  final listProduct = <ProductModel>[].obs;
  DateTime? backbuttonpressedTime;

  final count = 0.obs;
  @override
  void onInit() {
    addDataProduct();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;


  void addDataProduct() {
    listProduct.add(new ProductModel(
      name: "Biskuit",
      image: "assets/img/g.png",
      price: 6000,
      qty: 10,
    ));

    listProduct.add(new ProductModel(
      name: "Chips",
      image: "assets/img/g.png",
      price: 8000,
      qty: 9,
    ));

    listProduct.add(new ProductModel(
      name: "Oreo",
      image: "assets/img/g.png",
      price: 10000,
      qty: 0,
    ));

    listProduct.add(new ProductModel(
      name: "Tango",
      image: "assets/img/g.png",
      price: 12000,
      qty: 10,
    ));

    listProduct.add(new ProductModel(
      name: "Cokelat",
      image: "assets/img/g.png",
      price: 15000,
      qty: 10,
    ));

    update();

  }
}
